﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
[AddComponentMenu("Rendering/Per Camera Render Settings")]
public class PerCameraRenderSettings : MonoBehaviour {

    public bool fog = false;
    public FogMode fogMode = FogMode.ExponentialSquared;
    public Color fogColor = Color.gray;
    public float fogDensity = 0.005f;

    private bool _global_fog;
    private FogMode _global_fogMode;
    private Color _global_fogColor;
    private float _global_fogDensity;

    private bool dirty = false;


    public void Awake() {
        _global_fog = RenderSettings.fog;
        _global_fogColor = RenderSettings.fogColor;
        _global_fogMode = RenderSettings.fogMode;
        _global_fogDensity = RenderSettings.fogDensity;
    }

    public void OnPreRender() {
        if(!enabled)
            return;
        
        _global_fog = RenderSettings.fog;
        _global_fogColor = RenderSettings.fogColor;
        _global_fogMode = RenderSettings.fogMode;
        _global_fogDensity = RenderSettings.fogDensity;
        
        RenderSettings.fog = fog;
        RenderSettings.fogColor = fogColor;
        RenderSettings.fogMode = fogMode;
        RenderSettings.fogDensity = fogDensity;

        dirty = true;
    }

    public void OnPostRender() {
        if(!dirty)
            return;
        
        RenderSettings.fog = _global_fog;
        RenderSettings.fogColor = _global_fogColor;
        RenderSettings.fogMode = _global_fogMode;
        RenderSettings.fogDensity = _global_fogDensity;

        dirty = false;
    }
}
