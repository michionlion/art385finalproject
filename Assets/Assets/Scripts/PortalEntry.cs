﻿//========= Copyright 2016, HTC Corporation. All rights reserved. ===========

using UnityEngine;
using HTC.UnityPlugin.StereoRendering;

public class PortalEntry : MonoBehaviour
{
    public Collider playerCollider;

    public GameObject hmdRig;
    public GameObject hmdEye;
    public StereoRenderer stereoRendererThis;
    public StereoRenderer stereoRendererThat;

    /////////////////////////////////////////////////////////////////////////////////

    private float portalDelay = 0f;

    public void Update() {
        portalDelay -= Time.deltaTime;
    }

    public void OnTriggerEnter(Collider other) {
        Debug.Log("Triggered");
        // if hmd has collided with portal door
        if (other == playerCollider && portalDelay <= 0) {
            Debug.Log("is player");
            stereoRendererThis.shouldRender = false;
            stereoRendererThis.GetComponent<MeshRenderer>().enabled = false;

            // adjust rotation
            Quaternion rotEntryToExit = stereoRendererThis.anchorRot * Quaternion.Inverse(stereoRendererThis.canvasOriginRot);
            hmdRig.transform.rotation = rotEntryToExit * hmdRig.transform.rotation;

            // adjust position
            Vector3 posDiff = new Vector3(stereoRendererThis.stereoCameraHead.transform.position.x - hmdEye.transform.position.x,
                                          stereoRendererThis.stereoCameraHead.transform.position.y - hmdEye.transform.position.y,
                                          stereoRendererThis.stereoCameraHead.transform.position.z - hmdEye.transform.position.z);
            Vector3 camRigTargetPos = hmdRig.transform.position + posDiff;

            // assign the target position to camera rig
            hmdRig.transform.position = camRigTargetPos;

            stereoRendererThat.shouldRender = true;
            stereoRendererThat.GetComponent<PortalEntry>().StartPortalDelay();
            stereoRendererThat.GetComponent<MeshRenderer>().enabled = true;
        }
    }

    public void StartPortalDelay() {
        portalDelay = 1.25f;
    }
}
